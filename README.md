# poco conda recipe

Home: https://github.com/pocoproject/poco

Package license: BSL-1.0

Recipe license: BSD 2-Clause

Summary: POrtable COmponents C++ Libraries
